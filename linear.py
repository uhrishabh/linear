import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.pyplot
from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
matplotlib.pyplot.rcParams['agg.path.chunksize'] = 20000

def Line_equation(x, y):
    # number of observations/points
    n = np.size(x)
    # mean of x and y vector
    m_x, m_y = np.mean(x), np.mean(y)
    # calculating cross-deviation and deviation about x
    SS_xy = np.sum(y*x - n*m_y*m_x)
    SS_xx = np.sum(x*x - n*m_x*m_x)
    # calculating regression coefficients
    b_1 = SS_xy / SS_xx
    b_0 = m_y - b_1*m_x
    return(b_0, b_1)
 
def test(x,y,b):
    y_pred = b[0] + b[1]*x
    evalu(y_pred,y)
    plot(x,y,y_pred)
    
def plot(x, y, y_pred):
    plt.scatter(x, y, color = "m",marker = "o", s = 30)
    plt.plot(x, y_pred, color = "g")
    # putting labels
    plt.xlabel('x')
    plt.ylabel('y')
    # function to show plot
    plt.show()

#evaluation
def evalu(y_pred,y):
    print sqrt(mean_squared_error(y_pred,y))
    
def main():
    # observations
    Total=pd.read_csv('training.csv',delimiter=",")
    Total=Total.values
    print(Total.shape)
    X=Total[:,1:-1]
    print(X.shape)
    Y=Total[:,-1]
    print "done"
    #dividing the dataset
    X_train, X_test, Y1_train, Y1_test = train_test_split(X, Y, test_size=0.3, random_state=42)
    # estimating coefficients
    b = Line_equation(X_train[:,1], Y1_train)
    print("Estimated coefficients:\nb_0 = {}  \
          \nb_1 = {}".format(b[0], b[1]))
    # plotting regression line
    test(X_test[:,1], Y1_test, b)
 
if __name__ == "__main__":
    main()
